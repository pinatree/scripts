﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Detectors;

[RequireComponent(typeof(Collider2D))]
public class GroundChecker : ContactDetector<Transform>
{
	public bool IsGrounded => ObjectsInside.Count > 0;

	public Animator GroundedListenAnimator;
	public string GroundedPropertyName = "Grounded";

	private void Start()
	{

		if (ObjectsInside.Count == 0)
		{
			GroundedListenAnimator?.SetBool(GroundedPropertyName, false);
		}
		else
		{
			GroundedListenAnimator?.SetBool(GroundedPropertyName, true);
		}
	}

	protected override void AfterObjectsChanged()
	{
		GroundedListenAnimator?.SetBool(GroundedPropertyName, ObjectsInside.Count > 0);
	}
}
