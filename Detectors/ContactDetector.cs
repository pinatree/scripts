﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Detectors
{
	public enum SEARCH_MODE { UPPER_ONLY, CHILD_OBJECTS }
	//T == класс, который мы будем искать
	public abstract class ContactDetector<T> : MonoBehaviour
	{
		public SEARCH_MODE SearchMode = SEARCH_MODE.UPPER_ONLY;

		public UnityEvent ObjectsChanged;
		public UnityEvent<T> ObjectAdded;
		public UnityEvent<T> ObjectRemoved;
			
		public List<T> ObjectsInside = new List<T>();

		void OnTriggerEnter2D(Collider2D collision)
		{
			T found = default(T);

			if (SearchMode == SEARCH_MODE.UPPER_ONLY)
				found = collision.GetComponent<T>();
			else if(SearchMode == SEARCH_MODE.CHILD_OBJECTS)
				found = collision.GetComponentInChildren<T>();

			if (found != null)
			{
				ObjectsInside.Add(found);

				ObjectAdded?.Invoke(found);
				ObjectsChanged?.Invoke();

				AfterObjectsChanged();
			}
		}

		void OnTriggerExit2D(Collider2D collision)
		{
			T found = default(T);

			if (SearchMode == SEARCH_MODE.UPPER_ONLY)
				found = collision.GetComponent<T>();
			else if (SearchMode == SEARCH_MODE.CHILD_OBJECTS)
				found = collision.GetComponentInChildren<T>();

			if (found != null)
			{
				bool removed = ObjectsInside.Remove(found);

				if(removed == false)
					Debug.LogError("Выход незафиксированного объекта из детектора.");

				ObjectRemoved?.Invoke(found);
				ObjectsChanged?.Invoke();

				AfterObjectsChanged();
			}
		}

		protected abstract void AfterObjectsChanged();
	}
}
