﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public static class GameConfigurationProvider
{
	public static GameConfiguration GetGameConfiguration()
	{
		TextAsset textAsset = (TextAsset)Resources.Load("Configuration");
		XmlDocument xmldoc = new XmlDocument();
		xmldoc.LoadXml(textAsset.text);

		GameConfiguration configuration = (GameConfiguration)(new XmlSerializer(typeof(GameConfiguration)).Deserialize(new XmlNodeReader(xmldoc)));
		return configuration;
	}
}
