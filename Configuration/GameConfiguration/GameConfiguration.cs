﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class GameConfiguration
{
	public string DifficultyName { get; set; }

	public float Volume { get; set; }
}