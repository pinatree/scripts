﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class DifficultyProvider
{
	public static List<Difficulty> GetDifficulties()
	{
		TextAsset textAsset = (TextAsset)Resources.Load("Difficulties");
		XmlDocument xmldoc = new XmlDocument();
		xmldoc.LoadXml(textAsset.text);

		List<Difficulty> difficulties = (List<Difficulty>)(new XmlSerializer(typeof(List<Difficulty>)).Deserialize(new XmlNodeReader(xmldoc)));
		return difficulties;
	}
}
