﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Difficulty
{
	public string Name { get; set; }

	public PlayerModifier PlayerModifier { get; set; }

	public EnemyModifier EnemyModifier { get; set; }

	public float PriceRatio { get; set; }

	public float ExperienceModifier { get; set; }

	public float EpicModifier { get; set; }

	public string Description{ get; set; }
}


public abstract class Modifier
{
	public float HPRatio { get; set; }
	public float DamageRatio { get; set; }
	public float OverviewRatio { get; set; }
}

public class PlayerModifier : Modifier { }

public class EnemyModifier : Modifier { }