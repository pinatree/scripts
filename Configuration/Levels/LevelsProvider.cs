﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class LevelsProvider
{
	public static List<LevelInfo> GetLevels()
	{
		TextAsset textAsset = (TextAsset)Resources.Load("Levels");
		XmlDocument xmldoc = new XmlDocument();
		xmldoc.LoadXml(textAsset.text);

		List<LevelInfo> levels = (List<LevelInfo>)(new XmlSerializer(typeof(List<LevelInfo>)).Deserialize(new XmlNodeReader(xmldoc)));
		return levels;
	}
}
