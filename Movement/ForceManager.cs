﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class ForceManager : MonoBehaviour
{
	public float Velocity = 3f;
	public Rigidbody2D workingRigidbody;

	public Vector2 MovementVector;

	public void SetNewMoveVector(Vector2 moveVector)
	{
		if (moveVector == Vector2.zero)
			this.enabled = false;
		else
			this.enabled = true;

		MovementVector = moveVector;
	}

	public void FixedUpdate()
	{
		workingRigidbody.velocity = new Vector2(MovementVector.x * Velocity, workingRigidbody.velocity.y);
		//throw new System.NotImplementedException();
	}
}