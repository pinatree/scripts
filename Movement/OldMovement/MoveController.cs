﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public abstract class MoveController : MonoBehaviour
{
	public virtual bool MovingRight { get; set; }

	public virtual bool MovingLeft { get; set; }

	public virtual bool MovingDown { get; set; }

	public virtual bool MovingUp { get; set; }

	public virtual void Jump() { }
}

