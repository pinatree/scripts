﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Extensions;

public class PlayerMoveController : MoveController
{
	public GroundChecker GroundChecker;

	public Rigidbody2D PlayerRB;

	public Animator MoveListenAnimator;
	public string AnimatorMovePropertyName = "Running";

	private bool movingRight;
	public override bool MovingRight
	{
		get => movingRight;
		set
		{
			if(value != movingRight)
			{
				movingRight = value;
				UpdateMoving();
			}
		}
	}

	private bool movingLeft;
	public override bool MovingLeft
	{
		get => movingLeft;
		set
		{
			if (value != movingLeft)
			{
				movingLeft = value;
				UpdateMoving();
			}
		}
	}

	public float JumpPower = 250f;

	public float Speed;

	[SerializeField]
	float X_Movement;
	
    void FixedUpdate()
    {
		PlayerRB.velocity = new Vector2(X_Movement, PlayerRB.velocity.y);
    }

	void Start()
	{
		GroundChecker.ObjectsChanged.AddListener(UpdateMoving);
		UpdateMoving();
	}

	void OnDestroy()
	{
		GroundChecker.ObjectsChanged.RemoveListener(UpdateMoving);
	}

	void UpdateMoving()
	{
		if (MovingRight == MovingLeft)
		{
			MoveListenAnimator.SetBool(AnimatorMovePropertyName, false);
			this.enabled = false;

			return;
		}

		if (MovingRight)
			PlayerRB.transform.FlipRight();
		else
			PlayerRB.transform.FlipLeft();

		if (GroundChecker.IsGrounded == false)
		{
			MoveListenAnimator.SetBool(AnimatorMovePropertyName, false);
			this.enabled = false;

			return;
		}

		this.enabled = true;
		MoveListenAnimator.SetBool(AnimatorMovePropertyName, true);

		X_Movement = MovingRight ? Speed : (-1 * Speed);
	}

	public override void Jump()
	{
		if(GroundChecker.IsGrounded)
			PlayerRB.AddForce(new Vector2(0, JumpPower));
	}
}