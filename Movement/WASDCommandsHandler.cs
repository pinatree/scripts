﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class WASDCommandsHandler : MonoBehaviour
{
	public MoveManager MoveManager;

	private bool movingRight;
	public bool MovingRight
	{
		get => movingRight;
		set
		{
			if (value != movingRight)
			{
				movingRight = value;
				UpdateMoving();
			}
		}
	}

	private bool movingLeft;
	public bool MovingLeft
	{
		get => movingLeft;
		set
		{
			if (value != movingLeft)
			{
				movingLeft = value;
				UpdateMoving();
			}
		}
	}

	void UpdateMoving()
	{
		Vector2 CurrentMovingVector;

		if (MovingLeft == movingRight)
		{
			CurrentMovingVector = new Vector2();
		}
		else
		{
			if (MovingLeft)
				CurrentMovingVector = new Vector2(-1, 0);
			else
				CurrentMovingVector = new Vector2(1, 0);
		}

		MoveManager.SetMovement(CurrentMovingVector);
	}
}