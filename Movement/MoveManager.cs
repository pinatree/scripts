﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class MoveManager : MonoBehaviour
{
	public Rigidbody2D PlayerRB;

	public List<MotionRestriction> MotionRestriction = new List<MotionRestriction>();
	public ForceManager ForceManager;

	public Animator Animator;
	public string AnimMovePropertyName = "Running";
	
	Vector2 newMovement = new Vector2();
	bool TransformationHasBeenSuccess = true;

	public void SetMovement(Vector2 newMovement)
	{
		bool rotationSuccess = UpdateRotation(newMovement);
		bool movementSuccess = UpdateMovement(newMovement);

		TransformationHasBeenSuccess = rotationSuccess && movementSuccess;

		this.newMovement = newMovement;
	}

	bool UpdateRotation(Vector2 newMovement)
	{
		if (newMovement == Vector2.zero)
			return true;

		bool rotationMore0 = PlayerRB.transform.localScale.x > 0;
		bool movingMore0 = newMovement.x > 0;

		if (rotationMore0 == movingMore0)
			return true;

		bool rotationAvailable =
			(MotionRestriction.Count == 0) || MotionRestriction.All(restrict => restrict.CanFlip);

		if (!rotationAvailable)
			return false;

		PlayerRB.transform.localScale =
			new Vector3(PlayerRB.transform.localScale.x * -1,
			PlayerRB.transform.localScale.y,
			PlayerRB.transform.localScale.z);

		return true;
	}

	bool UpdateMovement(Vector2 newMovement)
	{
		bool movingAvailable =
			(MotionRestriction.Count == 0) || MotionRestriction.All(restrict => restrict.CanMove);

		if (!movingAvailable)
			return false;

		ForceManager.SetNewMoveVector(newMovement);
		return true;
	}
}