﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//IGNORES Z POSITION!!!
[RequireComponent(typeof(Transform))]
public class TransformSeeker : MonoBehaviour
{
	public Vector3 Offset;

	public Transform SeekTransform;

	private Transform SelfTransform;

	void Start()
	{
		SelfTransform = this.GetComponent<Transform>();
		UpdateEnabled();
	}

	void OnValidate() => UpdateEnabled();

	void UpdateEnabled() => this.enabled = (SeekTransform != null);

	void Update()
	{
		SelfTransform.position =
			new Vector3(SeekTransform.position.x + Offset.x, SeekTransform.position.y + Offset.y, SelfTransform.position.z);
	}
}
