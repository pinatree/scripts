﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.Scripts.Extensions
{
	public static class TransformExtensions
	{
		public static void FlipRight(this Transform self)
		{
			if (self.localScale.x < 0)
			{
				self.transform.localScale =
					new Vector3(self.transform.localScale.x * -1,
					self.transform.localScale.y,
					self.transform.localScale.z);
			}
		}

		public static void FlipLeft(this Transform self)
		{
			if (self.localScale.x > 0)
			{
				self.transform.localScale =
					new Vector3(self.transform.localScale.x * -1,
					self.transform.localScale.y,
					self.transform.localScale.z);
			}
		}
	}
}
