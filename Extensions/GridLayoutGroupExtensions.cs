﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public static class GridLayoutGroupExtensions
{
	public static void UpdateContainerHeight(this GridLayoutGroup gridLayoutGroup)
	{
		float rows = gridLayoutGroup.transform.childCount / gridLayoutGroup.constraintCount;
		if (gridLayoutGroup.transform.childCount % gridLayoutGroup.constraintCount != 0)
			rows++;

		RectTransform rect = gridLayoutGroup.GetComponent<RectTransform>();
		rect.sizeDelta = new Vector2(rect.sizeDelta.x,
			rows * (gridLayoutGroup.cellSize.y + gridLayoutGroup.spacing.y) + gridLayoutGroup.padding.bottom + gridLayoutGroup.padding.top);
	}

	public static void UpdateContainerWidth(this GridLayoutGroup gridLayoutGroup)
	{
		float cols = gridLayoutGroup.transform.childCount / gridLayoutGroup.constraintCount;
		if (gridLayoutGroup.transform.childCount % gridLayoutGroup.constraintCount != 0)
			cols++;

		var size_x = cols * (gridLayoutGroup.cellSize.x + gridLayoutGroup.spacing.x) + gridLayoutGroup.padding.left + gridLayoutGroup.padding.right;
		
		RectTransform rect = gridLayoutGroup.GetComponent<RectTransform>();

		rect.sizeDelta = new Vector2(size_x, rect.sizeDelta.y);
	}
}
