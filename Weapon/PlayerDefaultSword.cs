﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class PlayerDefaultSword : Weapon
{
	public int Damadge = 25;
	public HealthDetector HealthDetector;

	public string AttackAnimationName = "HeroAttack";
	public override void Attack()
	{		
		if(AttackListenAnimator.GetCurrentAnimatorStateInfo(0).IsName(AttackAnimationName) == false)
		{
			AttackListenAnimator.SetTrigger(AttackAnimTriggerName);
			foreach (var health in HealthDetector.ObjectsInside)
			{
				health.Damadge(Damadge);
			}
		}
	}

}