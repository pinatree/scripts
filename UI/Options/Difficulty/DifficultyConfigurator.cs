﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class DifficultyConfigurator : MonoBehaviour
{
	public GridLayoutGroup Layout;

	public DifficultySelector SelectorTemplate;

	void OnEnable()
	{
		var oldbuttons = Layout.GetComponentsInChildren<DifficultySelector>();
		foreach (var item in oldbuttons)
		{
			Destroy(item.gameObject);
		}

		var difficulties = DifficultyProvider.GetDifficulties();

		foreach (var item in difficulties)
		{
			DifficultySelector newDifficulty = Instantiate(SelectorTemplate);
			newDifficulty.Difficulty = item;
			newDifficulty.SetText(item.Name);
			newDifficulty.transform.SetParent(Layout.transform);
		}
		Layout.UpdateContainerWidth();
	}

	void SelectDifficulty(Difficulty difficulty)
	{
		Debug.Log("Данный функционал позже будет добавлен.");
	}
}
