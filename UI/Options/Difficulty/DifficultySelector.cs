﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class DifficultySelector : MonoBehaviour
{
	public Difficulty Difficulty;
	public Text CaptionText; 

	public event Action<Difficulty> OnExecute;

	public void Execute() => OnExecute?.Invoke(Difficulty);

	public void SetText(string text)
	{
		this.CaptionText.text = text;
	}
}
