﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;

public class SoundConfugurator : MonoBehaviour
{
	public Slider Slider;

	void OnEnable()
	{
		Slider.onValueChanged.AddListener(new UnityEngine.Events.UnityAction<float>(UpdateValue));
		Slider.value = GameConfigurationProvider.GetGameConfiguration().Volume;
	}

	void UpdateValue(float newValue)
	{
		Debug.Log("Функционал изменения файла конфигуркции будет добавлен позже");
	}
}