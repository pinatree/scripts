﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
	public LevelInfo SelectedLevel;

	public Text LevelInfoText;

	public Button LoadLevelButton;
	
	void Start()
	{
		if(LoadLevelButton != null)
		{
			LoadLevelButton.onClick.AddListener(new UnityEngine.Events.UnityAction(LoadLevel));
		}
		SetLevel(SelectedLevel);
	}

	public void SetLevel(LevelInfo levelInfo)
	{
		SelectedLevel = levelInfo;

		if (levelInfo == null)
		{
			this.gameObject.SetActive(false);
		}			
		else
		{
			this.gameObject.SetActive(true);
			ShowLevelInfo(levelInfo);
		}

	}
	void LoadLevel()
	{
		if(SelectedLevel != null)
		{
			SceneManager.LoadScene(SelectedLevel.ScenePath);
		}
	}

	void ShowLevelInfo(LevelInfo levelInfo)
	{
		if (LevelInfoText == null)
		{
			throw new NullReferenceException("Text field must be not null");
		}			
		else
		{
			LevelInfoText.text = levelInfo.Description;
		}
	}
}