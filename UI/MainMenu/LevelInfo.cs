﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LevelInfo
{
	public string Caption { get; set; }

	public string Description { get; set; }

	public string ScenePath { get; set; }
}