﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LevelContainer : MonoBehaviour
{
	public event Action<LevelInfo> OnExecute;

	public LevelInfo LevelInfo { get; set; }

	public void Execute() => OnExecute.Invoke(LevelInfo);

	public void SetButtonText(string newText)
	{
		Text result = this.GetComponentInChildren<Text>();
		if (result == null)
			throw new MissingComponentException("There is no component with type 'Text'");

		result.text = newText;
	}
}