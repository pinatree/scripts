﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelsScreen : MonoBehaviour
{
	//Шаблон кнопки, по которой будет вызываться уровень. Кнопка должна иметь компонент LevelContainer.
	//По нажатию на кнопку, она будет вызывать внутри себя событие OnExecute, а оно в свою очередь - заданный
	//нами CallBack
	public LevelContainer ContainerTemplate;

	//Список текущих кнопок с уровнями
	public LevelContainer[] Levels
	{
		get => throw new NotImplementedException();
	}

	//Экран с отображением выбранного уровня. Уровень выбирается, когда внутри кнопки вызывается событие
	//OnExecute(). Выбранный уровень передается в LevelSelector.
	public LevelSelector LevelSelector;

	//Место, где мы будем отображать кнопки с уровнями.
	public GridLayoutGroup LevelsContainer;

	//Перезагружаем список уровней и делаем прочие нужные вещи, как только становимся активными.
	//Этот метод вызывается в том числе и при обычной загрузке изначально активного компонента,
	//а не только при включении/выключении.
	void OnEnable()
	{
		UpdateLevels();
	}

	//Перезагрузка списка уровней. Оставлю этот метод публичным. Мало ли что может в будущем понадобиться,
	//а от лишней перезагрузки списка уровней катастрофы не случится.
	public void UpdateLevels()
	{
		//За выдачу уровней из xml-файла 'Levels.xml', находящегося в ресурсах, отвечает LevelsProvider.
		IEnumerable<LevelInfo> levels = LevelsProvider.GetLevels();

		//Заполняем этими уровнями контейнер с уровнями
		FillLevels(levels);

		//С помощью метода расширения, изменяем размер контейнера с уровнями
		LevelsContainer.UpdateContainerHeight();
	}

	void FillLevels(IEnumerable<LevelInfo> levels)
	{
		//Уничтожаем прошлые кнопки
		var oldbuttons = LevelsContainer.GetComponentsInChildren<LevelContainer>();//<MonoBehaviour>();
		foreach (var item in oldbuttons)
		{
			Destroy(item.gameObject);
		}

		//Заполняем новыми
		foreach (LevelInfo level in levels)
		{
			LevelContainer newContainer = Instantiate(ContainerTemplate);
			newContainer.OnExecute += SelectLevel;
			newContainer.LevelInfo = level;
			newContainer.SetButtonText(level.Caption);
			newContainer.transform.SetParent(LevelsContainer.transform);
		}
	}

	//Этот метод используется как CallBack для события OnExecute кнопок с уровнями
	void SelectLevel(LevelInfo level)
	{
		LevelSelector.SetLevel(level);
	}
}