﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using System;
using System.Linq;

[Serializable]
public class DialogueVariant
{
	public string Caption;

	public string Content;
}



[Serializable]
public struct DialogueState
{
	public GameObject caller;
	public bool wasCalling;
}


public class Dialogue : MonoBehaviour
{
	[SerializeField]
	List<DialogueState> dialogueStates = new List<DialogueState>();

	[SerializeField]
	List<DialogueVariant> DefaultDialogueVariants = new List<DialogueVariant>()
	{
		new DialogueVariant()
		{
			Caption = "Hello!",
			Content = "Hello, my dear friend!"
		},
		new DialogueVariant()
		{
			Caption = "Get out!",
			Content = "Get out of here!"
		},
	};

	[SerializeField]
	List<DialogueVariant> RepeatDialogueVariants = new List<DialogueVariant>()
	{
		new DialogueVariant()
		{
			Caption = "Nice to see you",
			Content = "Nice to see you!"
		},
		new DialogueVariant()
		{
			Caption = "I am going back",
			Content = "I am going back"
		},
	};

	public List<DialogueVariant> GetDialogueVarialts(GameObject asking)
	{
		if (dialogueStates.Any(x => x.caller == asking))
			return DefaultDialogueVariants;
		else
			return RepeatDialogueVariants;
	}

	public string ExecuteDialoguevarialts(GameObject asking, DialogueVariant variant)
	{
		if (variant.Caption == DefaultDialogueVariants[0].Caption)
		{
			dialogueStates.Add(
				new DialogueState()
				{
					caller = asking,
					wasCalling = true
				});

			return "Hello, bro!";
		}
		else if (variant.Caption == DefaultDialogueVariants[1].Caption)
		{
			Destroy(this.gameObject);
			return "I am going to the oblivion...";
		}
		else if (variant.Caption == RepeatDialogueVariants[0].Caption)
		{
			return "Thanks!";
		}
		else if (variant.Caption == RepeatDialogueVariants[1].Caption)
		{
			return "OK, good bye!";
		}
		else
		{
			return "I didn’t hear what you said ?";
		}
	}


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
