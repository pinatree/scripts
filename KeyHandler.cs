﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class KeyHandler : MonoBehaviour
{
	public GameObject Player;

	public KeyCode MoveLeftCode = KeyCode.A;
	public KeyCode MoveRightCode = KeyCode.D;

	public KeyCode JumpCode = KeyCode.Space;
	
	public KeyCode AttackCode = KeyCode.F;

	void Update()
	{
		if(Player != null)
		{
			WASDCommandsHandler mm = Player.GetComponentInChildren<WASDCommandsHandler>();
			if(mm != null)
			{
				if (Input.GetKeyDown(MoveLeftCode))
				{
					mm.MovingLeft = true;
				}
				if (Input.GetKeyDown(MoveRightCode))
				{
					mm.MovingRight = true;
				}

				if (Input.GetKeyUp(MoveLeftCode))
				{
					mm.MovingLeft = false;
				}
				if (Input.GetKeyUp(MoveRightCode))
				{
					mm.MovingRight = false;
				}

				//if (Input.GetKeyDown(JumpCode))
					//mm.Jump();
			}
			
			if (Input.GetKeyDown(AttackCode))
			{
				Weapon weapon = Player.GetComponentInChildren<Weapon>();
				weapon?.Attack();
			}
		}
	}
}

