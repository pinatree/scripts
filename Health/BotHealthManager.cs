﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class BotHealthManager : HealthManager
{
	public string OnDamadgeTriggerName = "Damadged";
	public Animator OnDamadgeAnimator;
	
	public override void Damadge(int dmg)
	{
		int newHealth = CurrentHealth - dmg;
		if (newHealth < 0)
		{
			newHealth = 0;
		}
		CurrentHealth = newHealth;

		HealthChanged.Invoke();
		Damadged.Invoke();
		if(CurrentHealth == 0)
		{
			HealhpointsEnded.Invoke();
		}

		//OnDamadgeAnimator?.SetTrigger(OnDamadgeTriggerName);
	}

	public override void Heal(int heal)
	{
		if (MaxHealth == CurrentHealth)
			return;

		int newHealth = CurrentHealth + heal;
		if (newHealth > this.MaxHealth)
		{
			newHealth = MaxHealth;
		}
		CurrentHealth = newHealth;

		HealthChanged.Invoke();
		Healed.Invoke();
	}
}