﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;

public abstract class HealthManager : MonoBehaviour
{
	public UnityEvent HealthChanged;
	public UnityEvent Healed;
	public UnityEvent Damadged;
	public UnityEvent HealhpointsEnded;

	public int CurrentHealth;

	public int MaxHealth;

	public abstract void Damadge(int dmg);

	public abstract void Heal(int heal);
}
