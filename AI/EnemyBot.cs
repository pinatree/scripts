﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class EnemyBot : MonoBehaviour
{
	public MoveController MoveController;

	public HealthDetector HealthDetector;

	void Start()
	{
		UpdateEnable();
		HealthDetector.ObjectsChanged.AddListener(UpdateEnable);
	}

	void FixedUpdate()
	{
		HealthManager foundHealth =
			HealthDetector.ObjectsInside.OrderBy(x =>
			Vector3.Distance(this.transform.position, x.transform.position)).First();

		var x_difference = this.transform.position.x - foundHealth.transform.position.x;

		if (x_difference < 0)
		{
			MoveController.MovingLeft = false;
			MoveController.MovingRight = true;
		}
		else
		{
			MoveController.MovingRight = false;
			MoveController.MovingLeft = true;
		}
	}

	void UpdateEnable()
	{
		if (HealthDetector.ObjectsInside.Count == 0)
		{
			MoveController.MovingRight = false;
			MoveController.MovingLeft = false;

			this.enabled = false;
		}
		else
			this.enabled = true;
	}
}