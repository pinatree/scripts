﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Assets.Scripts.Extensions;

using UnityEngine;

public class BotMoveController : MoveController
{
	public bool Flip = true;

	public Rigidbody2D BotRB;

	public Animator MoveListenAnimator;
	public string AnimatorMovePropertyName = "Walking";

	public float Speed;

	[SerializeField]
	float X_Movement;

	private bool movingRight;
	public override bool MovingRight
	{
		get => movingRight;
		set
		{
			if (value != movingRight)
			{
				movingRight = value;
				UpdateMoving();
			}
		}
	}

	private bool movingLeft;
	public override bool MovingLeft
	{
		get => movingLeft;
		set
		{
			if (value != movingLeft)
			{
				movingLeft = value;
				UpdateMoving();
			}
		}
	}

	void FixedUpdate()
	{
		BotRB.velocity = new Vector2(X_Movement, BotRB.velocity.y);
	}

	void UpdateMoving()
	{
		if (MovingRight == MovingLeft)
		{
			MoveListenAnimator.SetBool(AnimatorMovePropertyName, false);
			this.enabled = false;

			return;
		}

		if(Flip)
		{
			if (MovingLeft)
				BotRB.transform.FlipRight();
			else
				BotRB.transform.FlipLeft();
		}
		else
		{
			if (MovingRight)
				BotRB.transform.FlipRight();
			else
				BotRB.transform.FlipLeft();
		}

		this.enabled = true;
		MoveListenAnimator.SetBool(AnimatorMovePropertyName, true);

		X_Movement = MovingRight ? Speed : (-1 * Speed);
	}
}
